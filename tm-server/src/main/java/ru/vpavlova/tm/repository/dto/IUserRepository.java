package ru.vpavlova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.vpavlova.tm.api.repository.IRepository;
import ru.vpavlova.tm.dto.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    Optional<User> findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

}
