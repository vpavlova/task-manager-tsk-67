package ru.vpavlova.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vpavlova.tm.api.service.IReceiverService;
import ru.vpavlova.tm.configuration.LoggerConfiguration;
import ru.vpavlova.tm.listener.LogMessageListener;
import ru.vpavlova.tm.service.ActiveMQConnectionService;

public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final IReceiverService receiverService = context.getBean(ActiveMQConnectionService.class);
        receiverService.receive(context.getBean(LogMessageListener.class));
    }

}